package com.piter.sehat.domain.repository

import com.piter.sehat.domain.model.HomeEntity
import com.piter.sehat.domain.model.Product

interface AppRepository{
    suspend fun getHomes(): HomeEntity
    suspend fun saveProduct(product: Product): Long
    suspend fun selectAllProduct() : MutableList<Product>
}