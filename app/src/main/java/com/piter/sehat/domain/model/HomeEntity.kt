package com.piter.sehat.domain.model

import com.google.gson.annotations.SerializedName



data class HomeEntity(
    @SerializedName("data") var data: DataEntity
)
