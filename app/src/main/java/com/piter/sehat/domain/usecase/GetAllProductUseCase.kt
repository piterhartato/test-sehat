package com.piter.sehat.domain.usecase

import com.piter.sehat.data.mapper.CloudErrorMapper
import com.piter.sehat.domain.model.Product
import com.piter.sehat.domain.repository.AppRepository
import com.piter.sehat.domain.usecase.base.UseCase
import javax.inject.Inject

class GetAllProductUseCase @Inject constructor(
    errorUtil: CloudErrorMapper,
    private val appRepository: AppRepository
) : UseCase<MutableList<Product>>(errorUtil) {
    override suspend fun executeOnBackground(): MutableList<Product> {
        return appRepository.selectAllProduct()
    }
}
