package com.piter.sehat.domain.usecase

import com.piter.sehat.data.mapper.CloudErrorMapper
import com.piter.sehat.domain.model.HomeEntity
import com.piter.sehat.domain.repository.AppRepository
import com.piter.sehat.domain.usecase.base.UseCase
import javax.inject.Inject



class GetDataUseCase @Inject constructor(
    errorUtil: CloudErrorMapper,
    private val appRepository: AppRepository
) : UseCase<HomeEntity>(errorUtil) {
    override suspend fun executeOnBackground(): HomeEntity {
        return appRepository.getHomes()
    }

}