package com.piter.sehat.domain.model

import com.google.gson.annotations.SerializedName



data class DataEntity(
    @SerializedName("category") var categoryList: List<Category>,
    @SerializedName("productPromo") var productList: List<Product>
)
