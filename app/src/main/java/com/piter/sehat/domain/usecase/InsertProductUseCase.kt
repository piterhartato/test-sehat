package com.piter.sehat.domain.usecase

import com.piter.sehat.data.mapper.CloudErrorMapper
import com.piter.sehat.domain.model.Product
import com.piter.sehat.domain.repository.AppRepository
import com.piter.sehat.domain.usecase.base.UseCase
import javax.inject.Inject


class InsertProductUseCase @Inject constructor(
    errorUtil: CloudErrorMapper,
    private val appRepository: AppRepository
) : UseCase<Long>(errorUtil) {
    lateinit var product: Product
    override suspend fun executeOnBackground(): Long {
        return appRepository.saveProduct(product)
    }

}