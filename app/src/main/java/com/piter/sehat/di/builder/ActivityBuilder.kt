package com.piter.sehat.di.builder

import com.piter.sehat.ui.detail.DetailActivity
import com.piter.sehat.ui.home.MainActivity
import com.piter.sehat.ui.login.LoginActivity
import com.piter.sehat.ui.order.OrderActivity
import com.piter.sehat.ui.search.SearchActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [MainActivityProviders::class])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [DetailActivityProviders::class])
    abstract fun bindDetailActivity(): DetailActivity

    @ContributesAndroidInjector(modules = [OrderActivityProviders::class])
    abstract fun bindOrderActivity(): OrderActivity

    @ContributesAndroidInjector(modules = [SearchActivityProviders::class])
    abstract fun bindSearchActivity(): SearchActivity

    @ContributesAndroidInjector(modules = [LoginActivityProviders::class])
    abstract fun bindLoginActivity(): LoginActivity

}