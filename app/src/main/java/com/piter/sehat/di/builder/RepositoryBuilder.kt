package com.piter.sehat.di.builder

import com.piter.sehat.data.repository.AppRepoImp
import com.piter.sehat.domain.repository.AppRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryBuilder {
    @Binds
    abstract fun bindsRepository(repoImp: AppRepoImp): AppRepository
}