package com.piter.sehat.data.source.cloud

import com.piter.sehat.data.restful.ApiService
import com.piter.sehat.domain.model.HomeEntity

class CloudRepository(private val apIs: ApiService) : BaseCloudRepository {

    override suspend fun getHomes(): HomeEntity {
        return apIs.getHomes().await()
    }
}
