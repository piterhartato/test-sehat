package com.piter.sehat.data.repository

import com.piter.sehat.data.source.cloud.BaseCloudRepository
import com.piter.sehat.data.source.db.ProductDao
import com.piter.sehat.domain.model.HomeEntity
import com.piter.sehat.domain.model.Product
import com.piter.sehat.domain.repository.AppRepository
import javax.inject.Inject

class AppRepoImp @Inject constructor(
    private val cloudRepository: BaseCloudRepository,
    private val productDao: ProductDao
) : AppRepository {

    override suspend fun getHomes(): HomeEntity {
        return cloudRepository.getHomes()
    }

    override suspend fun saveProduct(product: Product): Long {
        productDao.insertProduct(product)
        return 0L
    }

    override suspend fun selectAllProduct(): MutableList<Product> {
        return productDao.selectAllProduct()
    }
}