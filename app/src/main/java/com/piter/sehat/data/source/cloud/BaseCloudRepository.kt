package com.piter.sehat.data.source.cloud

import com.piter.sehat.domain.model.HomeEntity

interface BaseCloudRepository {
   suspend fun getHomes(): HomeEntity
}