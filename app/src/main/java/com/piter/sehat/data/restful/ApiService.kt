package com.piter.sehat.data.restful

import com.piter.sehat.domain.model.HomeEntity
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface ApiService {

    @GET("home")
    fun getHomes(): Deferred<HomeEntity>

}