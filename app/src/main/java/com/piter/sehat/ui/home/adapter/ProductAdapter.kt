package com.piter.sehat.ui.home.adapter

import android.view.View
import com.mikepenz.fastadapter.items.AbstractItem
import com.piter.sehat.R
import com.piter.sehat.databinding.ProductItemRowBinding
import com.piter.sehat.domain.model.Product
import com.piter.sehat.ui.common.BindableListItemViewHolder



data class ProductAdapter(
    val viewModel: Product,
    val listener: EventListener
) :
    AbstractItem<ProductAdapter.ViewHolder>() {

    override val type: Int
        get() = hashCode()

    override val layoutRes: Int
        get() = R.layout.product_item_row

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    override fun bindView(holder: ViewHolder, payloads: MutableList<Any>) {
        super.bindView(holder, payloads)
        holder.binding.item = viewModel
        holder.binding.executePendingBindings()
        holder.itemView.setOnClickListener { listener.onClick(viewModel) }
    }

    class ViewHolder(itemView: View) : BindableListItemViewHolder<ProductItemRowBinding>(itemView = itemView)

    interface EventListener {
        fun onClick(itemViewModel: Product)
    }
}