package com.piter.sehat.ui.home.adapter

import android.view.View
import com.mikepenz.fastadapter.items.AbstractItem
import com.piter.sehat.R
import com.piter.sehat.databinding.CategoryItemRowBinding
import com.piter.sehat.domain.model.Category
import com.piter.sehat.ui.common.BindableListItemViewHolder


data class CategoryAdapter(
    val viewModel: Category,
    val listener: EventListener
) :
    AbstractItem<CategoryAdapter.ViewHolder>() {

    override val layoutRes: Int
        get() = R.layout.category_item_row

    override val type: Int
        get() = hashCode()

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    override fun bindView(holder: ViewHolder, payloads: MutableList<Any>) {
        super.bindView(holder, payloads)
        holder.binding.item = viewModel
        holder.binding.executePendingBindings()
        holder.itemView.setOnClickListener { listener.onClick(viewModel) }
    }

    class ViewHolder(itemView: View) : BindableListItemViewHolder<CategoryItemRowBinding>(itemView = itemView)

    interface EventListener {
        fun onClick(itemViewModel: Category)
    }
}