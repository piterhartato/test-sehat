package com.piter.sehat.ui.order.adapter

import android.view.View
import com.mikepenz.fastadapter.items.AbstractItem
import com.piter.sehat.R
import com.piter.sehat.databinding.OrderItemRowBinding
import com.piter.sehat.domain.model.Product
import com.piter.sehat.ui.common.BindableListItemViewHolder


data class OrderAdapter(
    val viewModel: Product
) :
    AbstractItem<OrderAdapter.ViewHolder>() {

    override val type: Int
        get() = hashCode()

    override val layoutRes: Int
        get() = R.layout.order_item_row

    override fun getViewHolder(v: View): ViewHolder =
        ViewHolder(v)

    override fun bindView(holder: ViewHolder, payloads: MutableList<Any>) {
        super.bindView(holder, payloads)
        holder.binding.item = viewModel
        holder.binding.executePendingBindings()
    }

    class ViewHolder(itemView: View) : BindableListItemViewHolder<OrderItemRowBinding>(itemView = itemView)

}