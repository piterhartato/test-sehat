package com.piter.sehat.ui.order

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.piter.sehat.domain.model.Product
import com.piter.sehat.domain.usecase.GetAllProductUseCase
import javax.inject.Inject



class OrderViewModel
@Inject constructor(
    private val getAllProductUseCase: GetAllProductUseCase
) : ViewModel() {
    val productList: MutableLiveData<List<Product>> by lazy { MutableLiveData<List<Product>>() }

    init {
        getAllProductUseCase.execute {
            onComplete {
                productList.value = it
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        getAllProductUseCase.unsubscribe()
    }
}