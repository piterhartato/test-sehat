package com.piter.sehat.ui.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.piter.sehat.domain.model.Product
import com.piter.sehat.domain.usecase.InsertProductUseCase
import javax.inject.Inject



class DetailViewModel
@Inject constructor(
    private val insertProductUseCase: InsertProductUseCase
) : ViewModel() {
    val status: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }

    fun insert(product: Product) {
        insertProductUseCase.product = product
        insertProductUseCase.execute {
            onComplete {
                status.value = true
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        insertProductUseCase.unsubscribe()
    }
}